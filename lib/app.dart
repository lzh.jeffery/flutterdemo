import 'package:flutter/material.dart';

// tabs
import './views/HomeTab.dart';
import './views/MessageTab.dart';
import './views/ProfileTab.dart';

class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => AppState();
}

class AppState extends State<App> {
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {

    void _setCurrentIndex(int index) {
      setState(() {
        _currentIndex = index;
      });
    }

    return Scaffold(
      body: _buildBody(),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        onTap: _setCurrentIndex,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Home')
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.mail),
            title: Text('Message')
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            title: Text('Profile')
          )
        ]
      )
    );
  }

  Widget _buildBody() {
    // return a wdggt in here
    List<Widget> bodyArr = [
      HomeTabView(),
      MessageTabView(),
      ProfileTabView()
    ];
    Widget currentWidget = _currentIndex < bodyArr.length
      ? bodyArr[_currentIndex]
      : Center(child: Text('Hello Flutter'));
    return currentWidget;
  }
}
