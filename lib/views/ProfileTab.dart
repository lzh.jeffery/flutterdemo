import 'package:flutter/material.dart';

class ProfileTabView extends StatelessWidget {

  AppBar getAppBar() {
    return AppBar(
      title: Text('Profile Tab')
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: getAppBar(),
      body: Center(
        child: Text('Profile')
      )
    );
  }
}
