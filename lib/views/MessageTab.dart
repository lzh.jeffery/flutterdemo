import 'package:flutter/material.dart';

import './Message.dart';

class MessageTabView extends StatelessWidget {
  
  AppBar getAppBar() {
    return AppBar(
      title: Text('Message Tab')
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: getAppBar(),
      body: MessageView()
    );
  }
}
