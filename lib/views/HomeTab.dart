import 'package:flutter/material.dart';

import './Home.dart';

class HomeTabView extends StatelessWidget {

  AppBar getAppBar() {
    return AppBar(
      title: Text('Home Tab')
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: getAppBar(),
      body: HomeView()
    );
  }
}