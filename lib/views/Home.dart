import 'package:flutter/material.dart';

// component
import '../components/randomWord.dart';

class HomeView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => HomeViewState();
}

class HomeViewState extends State<HomeView> {
  @override
  Widget build(BuildContext context) {
    return RandomWords();
  }
}
