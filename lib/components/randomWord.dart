import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';

import './listItem.dart';

class RandomWords extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => RandomWordsState();
}

class RandomWordsState extends State<RandomWords> {
  final _suggestions = <WordPair>[];
  final _biggerFont = const TextStyle(fontSize: 18.0);

  Widget _buildSuggestions(BuildContext context) {
    return ListView.builder(
      padding: const EdgeInsets.all(6.0),
      itemCount: 20,
      itemBuilder: (BuildContext context, int i) {
        if (i >= _suggestions.length) {
          _suggestions.addAll(generateWordPairs().take(10));
        }
        return _buildRow(_suggestions[i], context);
      }
    );
  }

  Widget _buildRow(WordPair pair, BuildContext context) {
    return ListItem(custom: CustomItem(pair.asPascalCase, _biggerFont));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildSuggestions(context)
    );
  }
}
