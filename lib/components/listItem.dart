import 'package:flutter/material.dart';


class CustomItem {
  final String title;
  final TextStyle fontStyle;

  CustomItem(this.title, this.fontStyle);
}

class ListItem extends StatelessWidget {

  final CustomItem custom;

  ListItem({Key key, @required this.custom}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Divider(),
        ListTile(
          title: Text(
            this.custom.title,
            style: this.custom.fontStyle
          ),
          onTap: () async {
            final result = await Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => new WillPopScope(
                onWillPop: () async {
                  return false;
                },
                child: Scaffold(
                  appBar: AppBar(
                    title: Text(this.custom.title),
                    leading: IconButton(
                      icon: Icon(Icons.chevron_left),
                      onPressed: () {
                        Navigator.pop(context, 'back');
                      }
                    )
                  ),
                  body: Center(
                    child: Text('screen page')
                  )
                )
              )
              )
            );
            if (result != null) {
              print(result);
            }
          }
        )
      ]
    );
  }

}
